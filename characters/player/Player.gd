extends KinematicBody2D

export var GRAVITY = 1000
export var MAX_FALL = 800
export var JUMP_FORCE = 500
export var health = 3 setget set_health

var weapon_laser = preload("res://characters/weapons/Laser/Laser2.tscn")
var weapon_lightning = preload("res://characters/weapons/Lightning/Lightning2.tscn")
var explosion = preload("res://effects/explosion/Explosion.tscn")

var motion = Vector2()

var double_jump = true

var previous_on_floor = false

var current_weapon = null

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("player")

	current_weapon = weapon_laser.instance()
	#current_weapon = weapon_lightning.instance()
	
	add_child(current_weapon)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	motion.y += GRAVITY * delta
	if motion.y > MAX_FALL:
		motion.y = MAX_FALL
	
	var vel = Vector2()
	
	vel.x = int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left"))
	#vel.y = int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up"))

	var current_motion = vel.normalized() * 200
	
	if vel.x > 0:
		$AnimationPlayer.play("Walk")
		$Sprite.scale = Vector2(0.3,0.3)
		current_weapon.scale = Vector2(current_weapon.soft_scale,current_weapon.soft_scale)
		current_weapon.rotation_degrees = -current_weapon.soft_rotation
		current_weapon.position.x = -current_weapon.soft_offset.x
	elif vel.x < 0:
		$AnimationPlayer.play("Walk")
		$Sprite.scale = Vector2(-0.3,0.3)
		current_weapon.scale = Vector2(-current_weapon.soft_scale,current_weapon.soft_scale)
		current_weapon.rotation_degrees = current_weapon.soft_rotation
		current_weapon.position.x = current_weapon.soft_offset.x
	else:
		$AnimationPlayer.play("Idle")
		#$Sprite.scale = Vector2(1,1)
		pass
		
	
	if Input.is_action_just_pressed("ui_accept"):
		if is_on_floor():
			motion.y = -JUMP_FORCE
		elif double_jump:
			motion.y = -JUMP_FORCE
			double_jump = false
		else:
			pass
			
	if Input.is_action_just_pressed("ui_select"):
		current_weapon.fire()
	
	if is_on_floor():
		double_jump = true
		previous_on_floor = true
		#motion.y = 0
	else:
		$AnimationPlayer.play("Jump")
		#print(""+str(previous_on_floor)+" "+str(motion.y))
		if previous_on_floor:
			previous_on_floor = false
			if motion.y > 200:
				motion.y = 0
		
	current_motion.y = motion.y
		
	motion.x = lerp(motion.x, current_motion.x, 0.2)
	
	move_and_slide(motion, Vector2(0,-1))
	
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.is_in_group("damage_tile"):
			#print("I collided with ", collision.collider.name)
			set_health(health - 1) 
	
	if is_on_floor():
		motion.y = 0
	
	get_parent().get_parent().get_node("GUI/Life").value = health
	
	#print(str(current_weapon.get_CD()))
	
	pass

func jump(value):
	motion.y = value
	pass
	
func bump(value):
	
	motion.x = value
	pass
	
func set_health(value):
	if $InvulnerabilityTimer.is_stopped():
		health = value
		$InvulnerabilityTimer.start()
	
		$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(0, 0, 0, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.interpolate_property($Sprite, "modulate", Color(0, 0, 0, 1), Color(1, 1, 1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.1)
		$Tween.start()
	
		if health <= 0:
			print("dead")
			var expl = explosion.instance()
			add_child(expl)
			expl.get_node("AnimationPlayer").play("explosion")
			pass
	pass

