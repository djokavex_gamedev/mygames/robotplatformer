extends Sprite

export var soft_offset = Vector2(0,0)
export var soft_scale = 1.0
export var soft_rotation = 90.0
export var soft_cd = 1.0

# Called when the node enters the scene tree for the first time.
func _ready():
	scale = Vector2(soft_scale, soft_scale)
	rotation_degrees = -soft_rotation
	position = soft_offset
	position.x = -position.x
	$CD.wait_time = soft_cd
	pass # Replace with function body.


func fire():
	if $CD.is_stopped():
		$CD.start()
		weapon_effect()
	pass

func weapon_effect():
	if position.x > 0:
		get_parent().bump(100)
	else:
		get_parent().bump(-100)
	get_parent().jump(-100)
	get_parent().get_node("Camera2D").shake_amount = 0.15
	pass


func _on_CD_timeout():
	pass # Replace with function body.

func get_CD():
	return $CD.get_time_left()
	pass
