extends "res://characters/weapons/Weapon.gd"

var lightning_bolt = preload("res://effects/Lightning/Lightning.tscn")

func weapon_effect():
	var light = lightning_bolt.instance()
	add_child(light)
	if get_parent().get_node("Sprite").scale.x > 0:
		light.create(global_position+Vector2(20,2), global_position+Vector2(200,0))
	else:
		light.create(global_position-Vector2(20,2), global_position-Vector2(200,0))
	
	if position.x > 0:
		get_parent().bump(100)
	else:
		get_parent().bump(-100)
	get_parent().jump(-50)
	get_parent().get_node("Camera2D").shake_amount = 0.15
	pass
