extends Sprite

var lightning_bolt = preload("res://effects/Lightning/Lightning.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func fire():
	var light = lightning_bolt.instance()
	add_child(light)
	if get_parent().get_node("Sprite").scale.x > 0:
		light.create(global_position+Vector2(20,2), global_position+Vector2(200,0))
	else:
		light.create(global_position-Vector2(20,2), global_position-Vector2(200,0))

	get_parent().get_node("Camera2D").shake_amount = 0.2
	pass

