extends "res://characters/weapons/Weapon.gd"

func weapon_effect():
	if $LaserBeam.is_casting == false:
		$LaserBeam.is_casting = true
		$Duration.start()
	pass


func _on_Duration_timeout():
	if $LaserBeam.is_casting == true:
		$LaserBeam.is_casting = false
	pass # Replace with function body.
