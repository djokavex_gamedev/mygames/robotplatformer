extends Sprite


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func fire():
	if $LaserBeam.is_casting == false:
		$LaserBeam.is_casting = true
		$Timer.start()
	pass


func _on_Timer_timeout():
	if $LaserBeam.is_casting == true:
		$LaserBeam.is_casting = false
	pass # Replace with function body.
