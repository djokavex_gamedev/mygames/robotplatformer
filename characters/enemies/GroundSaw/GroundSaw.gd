extends "res://characters/enemies/Enemy.gd"


func set_health(value):
	# Invulnerable
	pass
	
func compute_movement():
	$AnimationPlayer.play("Walk")
	
	if motion.x > 0:
		$BottomRayCast.position.x = 28
		$UpRayCast.position.x = -20
		$FrontRayCast.cast_to.x = 38
		$Sprite.scale.x = -1
	elif motion.x < 0:
		$BottomRayCast.position.x = -28
		$UpRayCast.position.x = 20
		$FrontRayCast.cast_to.x = -38
		$Sprite.scale.x = 1
	else:
		pass
	
	$BottomRayCast.force_raycast_update()
	if not $BottomRayCast.is_colliding():
		motion.x = -motion.x
	
	$FrontRayCast.force_raycast_update()
	if $FrontRayCast.is_colliding():
		if $FrontRayCast.get_collider().is_in_group("player"):
			#$FrontRayCast.get_collider().health = $FrontRayCast.get_collider().health - 1
			if motion.x > 0:
				$FrontRayCast.get_collider().bump(1000)
			else:
				$FrontRayCast.get_collider().bump(-1000)
		else:
			motion.x = -motion.x
			
	$UpRayCast.force_raycast_update()
	if $UpRayCast.is_colliding():
		if $UpRayCast.get_collider().is_in_group("player"):
			$UpRayCast.get_collider().health = $UpRayCast.get_collider().health - 1
			if motion.x > 0:
				$UpRayCast.get_collider().bump(-1000)
			else:
				$UpRayCast.get_collider().bump(1000)

	for i in get_slide_count():
		var collision = get_slide_collision(i)
		print("I collided with ", collision.collider.name)
		if collision.collider.is_in_group("player"):
			#print("I collided with ", collision.collider.name)
			var col = collision.collider
			col.health -= 1
			if motion.x > 0:
				col.bump(1000)
			else:
				col.bump(-1000)
				

