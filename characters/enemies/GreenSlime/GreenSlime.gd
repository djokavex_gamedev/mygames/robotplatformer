extends KinematicBody2D

export var SPEED = 150
export var health = 3 setget set_health

var motion = Vector2(SPEED, 100)

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("enemy")
	set_physics_process(false)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	
		
	if motion.x > 0:
		$BottomRayCast.position.x = 28
		$FrontRayCast.cast_to.x = 30
		$Sprite.scale.x = -1
		$AnimationPlayer.play("Walk")
	elif motion.x < 0:
		$BottomRayCast.position.x = -28
		$FrontRayCast.cast_to.x = -30
		$Sprite.scale.x = 1
		$AnimationPlayer.play("Walk")
	else:
		$AnimationPlayer.play("Idle")
	
	$BottomRayCast.force_raycast_update()
	if not $BottomRayCast.is_colliding():
		motion.x = -motion.x
	
	$FrontRayCast.force_raycast_update()
	if $FrontRayCast.is_colliding():
		if $FrontRayCast.get_collider().is_in_group("player"):
			$FrontRayCast.get_collider().health = $FrontRayCast.get_collider().health - 1
			if motion.x > 0:
				$FrontRayCast.get_collider().bump(1000)
			else:
				$FrontRayCast.get_collider().bump(-1000)
		else:
			motion.x = -motion.x
		
	#motion.x = lerp(motion.x)
	move_and_slide(motion, Vector2(0,-1))
	
	if is_on_floor():
		motion.y = 0
		
	if health >= 3:
		$GUI/LifeBar.visible = false
	else:
		$GUI/LifeBar.visible = true
	$GUI/LifeBar.value = health
		
	if $VisibilityEnabler2D.pause_animations:
		$VisibilityEnabler2D.pause_animations = false
		$VisibilityEnabler2D.freeze_bodies = false
		$VisibilityEnabler2D.pause_particles = false
		$VisibilityEnabler2D.pause_animated_sprites = false
		$VisibilityEnabler2D.process_parent = false
		$VisibilityEnabler2D.physics_process_parent = false
	
	pass

func set_health(value):
	health = value
	
	$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(0, 0, 0, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($Sprite, "modulate", Color(0, 0, 0, 1), Color(1, 1, 1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.1)
	$Tween.start()
	
	if health <= 0:
		queue_free()
	pass


func _on_VisibilityEnabler2D_screen_entered():
	

	#set_physics_process(true)

	pass # Replace with function body.
