extends "res://characters/enemies/Enemy.gd"

func compute_movement():
	if motion.x > 0:
		$BottomRayCast.position.x = 28
		$FrontRayCast.cast_to.x = 30
		$Sprite.scale.x = -1
		$AnimationPlayer.play("Walk")
	elif motion.x < 0:
		$BottomRayCast.position.x = -28
		$FrontRayCast.cast_to.x = -30
		$Sprite.scale.x = 1
		$AnimationPlayer.play("Walk")
	else:
		$AnimationPlayer.play("Idle")
	
	$BottomRayCast.force_raycast_update()
	if not $BottomRayCast.is_colliding():
		motion.x = -motion.x
	
	$FrontRayCast.force_raycast_update()
	if $FrontRayCast.is_colliding():
		if $FrontRayCast.get_collider().is_in_group("player"):
			$FrontRayCast.get_collider().health = $FrontRayCast.get_collider().health - 1
			if motion.x > 0:
				$FrontRayCast.get_collider().bump(1000)
			else:
				$FrontRayCast.get_collider().bump(-1000)
		else:
			motion.x = -motion.x
