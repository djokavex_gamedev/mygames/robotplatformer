extends KinematicBody2D

export var SPEED = 150
export var FALL_SPEED = 100
export var MAX_HEALTH = 3
export var health = 3 setget set_health

var motion = Vector2(SPEED, FALL_SPEED)

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("enemy")
	set_physics_process(false)
	motion = Vector2(SPEED, FALL_SPEED)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	compute_movement()
	move_enemy()
	update_gui()
	update_visibility_enabler()
	
	pass

func set_health(value):
	health = value
	
	$Tween.interpolate_property($Sprite, "modulate", Color(1, 1, 1, 1), Color(0, 0, 0, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Tween.interpolate_property($Sprite, "modulate", Color(0, 0, 0, 1), Color(1, 1, 1, 1), 0.1, Tween.TRANS_LINEAR, Tween.EASE_IN, 0.1)
	$Tween.start()
	
	if health <= 0:
		queue_free()
	pass

func compute_movement():
	if motion.x > 0:
		$Sprite.scale.x = -1
		$AnimationPlayer.play("Walk")
	elif motion.x < 0:
		$Sprite.scale.x = 1
		$AnimationPlayer.play("Walk")
	else:
		$AnimationPlayer.play("Idle")
	
	pass
	
func move_enemy():
	#motion.x = lerp(motion.x)
	move_and_slide(motion, Vector2(0,-1))
	
	if is_on_floor():
		motion.y = 0
	pass
func update_gui():
	if health >= MAX_HEALTH:
		$GUI/LifeBar.visible = false
	else:
		$GUI/LifeBar.visible = true
	$GUI/LifeBar.value = health
	pass

func update_visibility_enabler():
	if $VisibilityEnabler2D.pause_animations:
		$VisibilityEnabler2D.pause_animations = false
		$VisibilityEnabler2D.freeze_bodies = false
		$VisibilityEnabler2D.pause_particles = false
		$VisibilityEnabler2D.pause_animated_sprites = false
		$VisibilityEnabler2D.process_parent = false
		$VisibilityEnabler2D.physics_process_parent = false
	pass
