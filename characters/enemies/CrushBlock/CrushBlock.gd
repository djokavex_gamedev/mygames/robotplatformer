extends "res://characters/enemies/Enemy.gd"


func compute_movement():
	if is_on_floor():
		motion.y = -FALL_SPEED * 0.9
		$AnimationPlayer.play("Walk")
	else:
		if not $AnimationPlayer.is_playing():
			$AnimationPlayer.play("Idle")
		
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.is_in_group("player"):
			#print("I collided with ", collision.collider.name)
			var col = collision.collider
			col.health -= 1
	pass

func set_health(value):
	# Invulnerable
	pass


func _on_CrushTimer_timeout():
	motion.y = FALL_SPEED
	pass # Replace with function body.
